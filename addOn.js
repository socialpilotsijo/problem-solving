const oldAddonsData = [
  {
    name: "team-member",
    count: 2,
    price: 20,
  },
  {
    name: "account",
    count: 0,
    price: 0,
  },
  {
    name: "feed",
    count: 2,
    price: 14,
  },
];

// const oldAddonsData = [];

const addons = [
  {
    name: "team-member",
    count: 1,
    price: 10,
  },
  {
    name: "account",
    count: 1,
    price: 5,
  },
  {
    name: "feed",
    count: 1,
    price: 7,
  },
];

let newAddonsData = [
  {
    name: "team-member",
    count: 0,
  },
  {
    name: "account",
    count: 3,
  },
  {
    name: "None",
    count: 2,
  },
  {
    name: "feed",
    count: 6,
  },
];

const finalData = [];
const message = [];
let oldPrice = 0;
let newPrice = 0;

// Getting the actual availabe addOntypes
const addOnTypes = addons.map((value, index) => value.name);

//Removing invalid addOn from the newAddons

newAddonsData = newAddonsData.filter((value, index) => {
  const valKeys = Object.keys(value);
  if (valKeys.includes("name")) {
    if (valKeys.includes("count")) {
      if (addOnTypes.includes(value["name"])) {
        return true;
      } else {
        message.push(`AddOnType \"${value["name"]}\" doesn't exist`);
      }
    } else {
      message.push(`There is no key "count" for object at index ${index}`);
    }
  } else {
    message.push(`There is no key "name" for object at index ${index}`);
  }
});

// Checking if there are any data left after the removal of non-existent addOns
if (newAddonsData.length === 0) {
  message.push("No changes made to current plan");
} else {
  const flag = oldAddonsData.length;
  newAddonsData.forEach((value) => {
    const id = addOnTypes.findIndex((val) => val === value["name"]);
    const price = addons[id]["price"] * value["count"];
    newPrice = newPrice + price;
    //Current count of Add On
    const newCount = value["count"];
    // This means that there is existing data
    if (flag) {
      const oldTemp = oldAddonsData.find(
        (vals) => vals["name"] === value["name"]
      );
      //If oldAddOn had current addOn.
      if (oldTemp !== undefined) {
        const oldCount = oldTemp["count"];

        //If the addOns are removed
        if (oldCount > newCount) {
          //Checking if newAdd on count is 0 or not
          if (newCount !== 0) {
            message.push(
              `Removing ${oldCount - newCount} AddOn instance Of ${
                value["name"]
              }. Total ${value["name"]} ==> ${newCount}`
            );
          } else {
            message.push(`Removing all AddOn instance Of ${value["name"]}`);
            console.log(price);
          }
        } else {
          //If addOns are added

          if (newCount !== 0 && newCount !== oldCount) {
            message.push(
              `Adding ${newCount - oldCount} AddOn instance of ${
                value["name"]
              }. Total ${value["name"]} ==> ${newCount}`
            );
          }
        }
      } else {
        if (newCount !== 0) {
          message.push(
            `Adding ${newCount} AddOn instance of ${value["name"]}. Total ${value["name"]} ==> ${newCount}`
          );
        }
      }
    } else {
      if (newCount !== 0) {
        message.push(
          `Adding ${newCount} AddOn instance of ${value["name"]}. Total ${value["name"]} ==> ${newCount}`
        );
      }
    }
    const temp = {
      name: value["name"],
      count: newCount,
      price,
    };
    finalData.push(temp);
  });
}

console.log("\nMessages\n");

let i = 1;
message.forEach((value) => {
  console.log(`${i++}. ==> ${value}`);
});

const finalDataItems = finalData.map((value) => value["name"]);

//Adding addOns that were already existing
oldAddonsData.forEach((value) => {
  oldPrice = oldPrice + value["price"];
  if (!finalDataItems.includes(value["name"])) {
    finalData.push(value);
  }
});

//If we want to remove all the addOn details with zero instance

// finalData = finalData.filter((value) => value["count"] !== 0);

console.log("\n\nFinal Data\n");
console.log(finalData);

if (oldPrice > newPrice)
  console.log(`\n${oldPrice - newPrice} $ has been credited to your account`);
else if (oldPrice < newPrice)
  console.log(`\n${newPrice - oldPrice} $ has been debited from your account`);
